#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "scene.h"
#include "settings.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_Ok_clicked();

    void on_Cansel_clicked();

    void on_pushButton_clicked();

private:
    Ui::Dialog *ui;
    Scene *Gamescene;
    Settings *GameSettings;
    Scene *GameScene;
};

#endif // DIALOG_H
