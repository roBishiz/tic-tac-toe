/********************************************************************************
** Form generated from reading UI file 'scene.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCENE_H
#define UI_SCENE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>

QT_BEGIN_NAMESPACE

class Ui_Scene
{
public:
    QGridLayout *gridLayout;
    QGraphicsView *graphicsView;

    void setupUi(QDialog *Scene)
    {
        if (Scene->objectName().isEmpty())
            Scene->setObjectName(QStringLiteral("Scene"));
        Scene->resize(400, 300);
        gridLayout = new QGridLayout(Scene);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        graphicsView = new QGraphicsView(Scene);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));

        gridLayout->addWidget(graphicsView, 0, 0, 1, 1);


        retranslateUi(Scene);

        QMetaObject::connectSlotsByName(Scene);
    } // setupUi

    void retranslateUi(QDialog *Scene)
    {
        Scene->setWindowTitle(QApplication::translate("Scene", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Scene: public Ui_Scene {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCENE_H
