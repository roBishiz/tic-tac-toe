#ifndef REGACC_H
#define REGACC_H

#include <QDialog>

namespace Ui {
class regAcc;
}

class regAcc : public QDialog
{
    Q_OBJECT

public:
    explicit regAcc(QWidget *parent = nullptr);
    ~regAcc();

private slots:
    void on_Cansel_clicked();

    void on_Ok_clicked();

private:
    Ui::regAcc *ui;
};

#endif // REGACC_H
