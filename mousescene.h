#ifndef MOUSESCENE_H
#define MOUSESCENE_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <iostream>
#include <queue>
using namespace std;

class mousescene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit mousescene(QObject *parent = 0);
    ~mousescene();
    struct node
    {
        int x, y;
        //node *link;
    };
    queue<node> myQueue;
    node *plant, *newPlant;
    int x, y, z, n;
    bool turn = 0;
    int **a;
    void Mas(int x, int y, int z){
        GridPaint(x,y,this);
        turn = 0;
        this -> x = x;
        this -> y = y;
        this -> z = z;
        int **a = new int *[x];
        for(int i = 0; i < x; i++){
            a[i] = new int [y];
        }
        for(int i = 0; i < x; i++){
            for(int j = 0; j < y; j++){
                a[i][j] = 0;
            }
        }
        this->a=a;
    }
    int check();
    //{
//        int x1 = x, y1 = y, z1 = 0;
//        if(a[x][y] == 1){
//            while(a[x1][y1] == 1){
//                x1++;
//                y1++;
//                z1++;
//                if(z1 == z){
//                    int mas[4] = {x,y,x1,x1};
//                    return mas[4];            //end
//                }
//            }
//        }
//        else{

//        }
//    }
    void paint(QPointF);
    void GridPaint(int, int, QGraphicsScene *);
signals:
    void signalTargetCoordinate(QPointF point);

//public slots:
//    void paint(QPointF point);
//    void GridPaint(int x, int y, QGraphicsScene *scene);

private:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // MOUSESCENE_H
