#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //connect(sceneMouse, &mousescene::signalTargetCoordinate, sceneMouse, &mousescene::paint);
    //Dialog test(this);
    //test.exec();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::reEnterScene(mousescene *scene){
    sceneMouse = scene;
    ui->graphicsView->setScene(scene);
    connect(sceneMouse, &mousescene::signalTargetCoordinate, sceneMouse, &mousescene::paint);
    //qDebug() << "BLABLA";
}

void MainWindow::on_actionNew_game_triggered()
{
    Dialog test(this);
    test.exec();
}
