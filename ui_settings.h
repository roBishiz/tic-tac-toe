/********************************************************************************
** Form generated from reading UI file 'settings.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGS_H
#define UI_SETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_Settings
{
public:
    QGridLayout *gridLayout;
    QLabel *label_1;
    QSlider *Stroke_lenght;
    QSpinBox *spinBox_1;
    QLabel *label_2;
    QSlider *Slider_width;
    QSpinBox *spinBox_2;
    QLabel *label_3;
    QSlider *Slider_height;
    QSpinBox *spinBox_3;
    QPushButton *pushButton;
    QLineEdit *GameName;
    QComboBox *comboBox;
    QPushButton *pushButton_2;

    void setupUi(QDialog *Settings)
    {
        if (Settings->objectName().isEmpty())
            Settings->setObjectName(QStringLiteral("Settings"));
        Settings->resize(343, 195);
        gridLayout = new QGridLayout(Settings);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_1 = new QLabel(Settings);
        label_1->setObjectName(QStringLiteral("label_1"));

        gridLayout->addWidget(label_1, 2, 0, 1, 1);

        Stroke_lenght = new QSlider(Settings);
        Stroke_lenght->setObjectName(QStringLiteral("Stroke_lenght"));
        Stroke_lenght->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(Stroke_lenght, 2, 1, 1, 1);

        spinBox_1 = new QSpinBox(Settings);
        spinBox_1->setObjectName(QStringLiteral("spinBox_1"));

        gridLayout->addWidget(spinBox_1, 2, 2, 1, 1);

        label_2 = new QLabel(Settings);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 3, 0, 1, 1);

        Slider_width = new QSlider(Settings);
        Slider_width->setObjectName(QStringLiteral("Slider_width"));
        Slider_width->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(Slider_width, 3, 1, 1, 1);

        spinBox_2 = new QSpinBox(Settings);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));

        gridLayout->addWidget(spinBox_2, 3, 2, 1, 1);

        label_3 = new QLabel(Settings);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 4, 0, 1, 1);

        Slider_height = new QSlider(Settings);
        Slider_height->setObjectName(QStringLiteral("Slider_height"));
        Slider_height->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(Slider_height, 4, 1, 1, 1);

        spinBox_3 = new QSpinBox(Settings);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));

        gridLayout->addWidget(spinBox_3, 4, 2, 1, 1);

        pushButton = new QPushButton(Settings);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 5, 0, 1, 1);

        GameName = new QLineEdit(Settings);
        GameName->setObjectName(QStringLiteral("GameName"));

        gridLayout->addWidget(GameName, 0, 0, 1, 3);

        comboBox = new QComboBox(Settings);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        gridLayout->addWidget(comboBox, 1, 0, 1, 3);

        pushButton_2 = new QPushButton(Settings);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gridLayout->addWidget(pushButton_2, 5, 2, 1, 1);

        QWidget::setTabOrder(GameName, comboBox);
        QWidget::setTabOrder(comboBox, Stroke_lenght);
        QWidget::setTabOrder(Stroke_lenght, Slider_width);
        QWidget::setTabOrder(Slider_width, Slider_height);
        QWidget::setTabOrder(Slider_height, spinBox_1);
        QWidget::setTabOrder(spinBox_1, spinBox_2);
        QWidget::setTabOrder(spinBox_2, spinBox_3);
        QWidget::setTabOrder(spinBox_3, pushButton);
        QWidget::setTabOrder(pushButton, pushButton_2);

        retranslateUi(Settings);

        QMetaObject::connectSlotsByName(Settings);
    } // setupUi

    void retranslateUi(QDialog *Settings)
    {
        Settings->setWindowTitle(QApplication::translate("Settings", "Dialog", nullptr));
        label_1->setText(QApplication::translate("Settings", "Stroke lenght", nullptr));
        label_2->setText(QApplication::translate("Settings", "Slider width", nullptr));
        label_3->setText(QApplication::translate("Settings", "Slider height", nullptr));
        pushButton->setText(QApplication::translate("Settings", "Ok", nullptr));
        pushButton_2->setText(QApplication::translate("Settings", "Cansel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Settings: public Ui_Settings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGS_H
