/********************************************************************************
** Form generated from reading UI file 'regacc.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGACC_H
#define UI_REGACC_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_regAcc
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QLabel *label;
    QVBoxLayout *verticalLayout;
    QLineEdit *Login;
    QLineEdit *Password;
    QHBoxLayout *horizontalLayout;
    QPushButton *Ok;
    QPushButton *Cansel;

    void setupUi(QDialog *regAcc)
    {
        if (regAcc->objectName().isEmpty())
            regAcc->setObjectName(QStringLiteral("regAcc"));
        regAcc->resize(219, 116);
        gridLayout = new QGridLayout(regAcc);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_2 = new QLabel(regAcc);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_3->addWidget(label_2);

        label = new QLabel(regAcc);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_3->addWidget(label);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        Login = new QLineEdit(regAcc);
        Login->setObjectName(QStringLiteral("Login"));

        verticalLayout->addWidget(Login);

        Password = new QLineEdit(regAcc);
        Password->setObjectName(QStringLiteral("Password"));

        verticalLayout->addWidget(Password);


        horizontalLayout_2->addLayout(verticalLayout);


        gridLayout_2->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        Ok = new QPushButton(regAcc);
        Ok->setObjectName(QStringLiteral("Ok"));

        horizontalLayout->addWidget(Ok);

        Cansel = new QPushButton(regAcc);
        Cansel->setObjectName(QStringLiteral("Cansel"));

        horizontalLayout->addWidget(Cansel);


        gridLayout_2->addLayout(horizontalLayout, 1, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(regAcc);

        QMetaObject::connectSlotsByName(regAcc);
    } // setupUi

    void retranslateUi(QDialog *regAcc)
    {
        regAcc->setWindowTitle(QApplication::translate("regAcc", "Dialog", nullptr));
        label_2->setText(QApplication::translate("regAcc", "Login:", nullptr));
        label->setText(QApplication::translate("regAcc", "Password:", nullptr));
        Ok->setText(QApplication::translate("regAcc", "Ok", nullptr));
        Cansel->setText(QApplication::translate("regAcc", "Cansel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class regAcc: public Ui_regAcc {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGACC_H
