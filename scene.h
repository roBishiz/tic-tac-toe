#ifndef SCENE_H
#define SCENE_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include "mousescene.h"

namespace Ui {
class Scene;
}

class Scene : public QDialog
{
    Q_OBJECT

public:
    explicit Scene(QWidget *parent = nullptr);
    mousescene *Gamescene;
    //int x, y, z;
    //Scene();
    //Scene(int x, int y, int z){
    //    sceneMouse->z = z;
    //    sceneMouse->Mas(x,y);
    //    sceneMouse->turn = 0;
    //    sceneMouse->GridPaint(x,y,sceneMouse);

    //    connect(sceneMouse, &mousescene::signalTargetCoordinate, sceneMouse, &mousescene::paint);
    //}

    ~Scene();
public slots:
    void reEnterScene(mousescene *scene);
private:
    Ui::Scene *ui;
    mousescene *sceneMouse;
    QGraphicsScene *scene;
};

#endif // SCENE_H
