#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include "logadmin.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->Password->setEchoMode(QLineEdit::Password);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_Ok_clicked()
{
    bool flag = false;
    QString Log = "Boris", Pass = "12345", playerName, playerPassword;
    QFile file("file.dat");
    if(!file.open(QIODevice::ReadOnly))
        return;
    QDataStream in(&file);
    do
    {
        in >> playerName;
        in >> playerPassword;
        //qDebug() << playerName;
        //qDebug() << playerPassword;
        if (playerName == ui->Login->text())
            flag = true;
    }
    while (playerName != ui->Login->text() && !in.atEnd());
    if(flag)
    {
        if (QString::compare(playerPassword, ui->Password->text(), Qt::CaseSensitive) == 0)
        {
            int reply = QMessageBox::question(this, "Login", "You are logged in.\nWant to start the game?", QMessageBox::Yes, QMessageBox::No);
            if(reply == 16384){
                close();
                GameSettings = new Settings();
                connect(GameSettings, SIGNAL(EnterScene(mousescene *)), parent(), SLOT(reEnterScene(mousescene *)));
                GameSettings->exec();
            }
            else
            {
                ui->Password->setText("");
            }
            return;
        }
    }
    ui->Password->setText("");
    QMessageBox::warning(this, "Error!", "Wrong password or login");
    return;

    //    if((Log == playerName && Pass == playerPassword) || Log == '1')
    //    {
    //        int reply = QMessageBox::question(this, "Login", "You are logged in.\nWant to start the game?", QMessageBox::Yes,QMessageBox::No);

    //        //QMessageBox msgBox;
    //        //QPushButton *connectButton = msgBox.addButton(tr("Yes"), QMessageBox::ActionRole);
    //        //QPushButton *abortButton = msgBox.addButton(QMessageBox::No);

    //        //msgBox.exec();

    //        //if (msgBox.clickedButton() == connectButton) {
    //        // connect
    //        //} else if (msgBox.clickedButton() == abortButton) {
    //        // abort
    //        //}

    //        if(reply == 16384){
    //            close();
    //            GameSettings = new Settings();
    //            //GameScene = new Scene();
    //            connect(GameSettings, SIGNAL(EnterScene(mousescene *)), parent(), SLOT(reEnterScene(mousescene *)));
    //            //qDebug() << 2.1;
    //            GameSettings->exec();
    //            //connect(GameSettings, &Settings::EnterScene, GameScene, &Scene::reEnterScene);
    //            //qDebug() << 2.2;
    //            //GameScene = new Scene();
    //            //qDebug() << 2.3;
    //            //connect(GameSettings, &Settings::EnterScene, GameScene, &Scene::reEnterScene);
    //            //qDebug() << 2.4;
    //            //GameScene->exec();

    //            //qDebug() << 2.2 <<  GameScene->Gamescene->x << GameScene->Gamescene->y;
    //            //Gamescene = new Scene();
    //            //Gamescene->setWindowTitle();
    //        }
    //        else{
    //            //ui->Login->selectAll();
    //            //ui->Login->setText("");
    //            ui->Password->setText("");
    //            //QString fileName = QFileDialog::getSaveFileName(this, tr("Save NotebookAs Text"),tr(""), tr("*.txt") );
    //        }
    //    }
    //    else
    //    {
    //        //QMessageBox::Critical(this, "Login", "You are logged in.\nWant to start the game?");
    //        QMessageBox::warning(this, "Error!", "Wrong password or login");
    //        ui->Password->setText("");
    //        //QMessageBox Login;
    //        //Login.setWindowTitle("Error");
    //        //Login.setText("Wrong password or login");
    //        //Login.exec();

    //    }
    //ui->Login->setText("Da");
    //ui->lineEdit->text);
}

void Dialog::on_Cansel_clicked()
{
    close();
}

void Dialog::on_pushButton_clicked()
{
    logAdmin Login(this);
    Login.setWindowTitle("Login in admin");
    Login.exec();
}
