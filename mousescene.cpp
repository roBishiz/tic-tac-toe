#include "mousescene.h"
#include <QDebug>

mousescene::mousescene(QObject *parent) :
    QGraphicsScene()
{
    Q_UNUSED(parent);
}

void mousescene::paint(QPointF point)
{
    n++;
    int x = point.x(), y = point.y();
    if(x > this->x*40 || y < -this->y*40 || x < 0 || y > 0)
    {
        return;
    }

    if(this->a[x/40][abs(y)/40] != 0){
        return;
    }

    //qDebug() << abs(y)/40+1 << x/40+1 << "If 1 to Ellipse if 0 cross" << turn;

    x = (x/40)*40+5;
    y = (y/40)*40-35;

    if(this->turn == 1){
        this->addEllipse(x,y,30,30);
        this->turn = 0;
        this->a[x/40][abs(y)/40] = 2;
    }
    else{
        this->addLine(x,y,x+30,y+30,QPen(Qt::black));
        this->addLine(x,y+30,x+30,y,QPen(Qt::black));
        this->turn = 1;
        this->a[x/40][abs(y)/40] = 1;
    }

//    if(n == 1){
        plant = new node;
        plant->x = x/40;
        plant->y = abs(y)/40;
        myQueue.push(*plant);
        
        while(!myQueue.empty())
        {
            qDebug() << "myQueue.front().x + 1 << " << myQueue.front().x + 1 << "myQueue.front().y + 1 << " << myQueue.front().y + 1;
            //myQueue.pop();
        }
        
        *plant = myQueue.front();
        
//        plant = new node;
//        plant->link = nullptr;
//        plant->x = x/40;
//        plant->y = abs(y)/40;
//        plant->n = 1;
//    }
//    else
//    {
//        newPlant = new node;
//        newPlant->link = plant;
//        newPlant->x = x/40;
//        newPlant->y = abs(y)/40;
//        newPlant->n = n;
//        plant = newPlant;
//    }
    qDebug() << abs(y)/40 << x/40 << this->x << this->y;
    this->check();
}

void mousescene::GridPaint(int x, int y, QGraphicsScene *scene)
{
    for(int i = 0; i < x+1; i++){
        scene->addLine(i*40,0,i*40,-y*40,QPen(Qt::black));
    }
    for(int i = 0; i < y+1; i++){
        scene->addLine(0,-i*40,x*40,-i*40,QPen(Qt::black));
    }
    //scene->addLine(0,-250,0,250,QPen(Qt::red));
    //scene->addLine(-250,0,250,0,QPen(Qt::red));
}

int mousescene::check()
{
    int rx, rz, //Right
            rtx, rty, rtz, //Right top
            ty, tz, //Top
            ltx, lty, ltz, //Left top
            tape;
    //int j = 0; j < y; j++
    for(int j = 0; j < y; j++)
    {
        for(int i = 0; i < x; i++)
        {
            //qDebug() << "if(a[i][j] == 1 || a[i][j] == 2)" << x1 << y1;
            //qDebug() << "i, j << " << i << j;
            //qDebug() << "a[i][j] << " << a[i][j];
            if(a[i][j] == 1 || a[i][j] == 2)
            {
                //qDebug() << "i, j << " << i << j;
                tape = a[i][j];
                //qDebug() << "tape << " << tape;
                rz = 1;
                rtz = 1;
                tz = 1;
                ltz = 1;
                rx = rtx = ltx = i;
                rty = ty = lty = j;

                rx++;
                //qDebug() << "rz << " << rz;
                //qDebug() << "z << " << z;
                //qDebug() << "rx << " << rx;
                //qDebug() << "rz << " << rz;
                //qDebug() << "z << " << z;
                while(/*rz != z && */rx < x && a[rx][j] == tape)
                {
                    rz++;
                    rx++;
                    //qDebug() << "rz/end << " << rz;
                }
                //qDebug() << "rz << " << rz;
                //qDebug() << "z << " << z;
                if(rz == z)
                {
                    for(int ir = i; ir < rx; ir++)
                    {
                        //qDebug() << "ir, j << " << ir << j;
                        a[ir][j] = 3;
                    }
                    //() << "i, j << " << i << j;
                    this->addLine(i*40,-(j*40+20),rx*40,-(j*40+20),QPen(Qt::black));
                }

                rtx++;
                rty++;
                while(/*rtz != z && */rtx < x && rty < y && a[rtx][rty] == tape)
                {
                    rtz++;
                    rtx++;
                    rty++;
                }
                if(rtz == z)
                {
                    for(int irt = i, jrt = j; irt < rtx; irt++, jrt++)
                    {
                        //qDebug() << "irt, jrt << " << irt << jrt;
                        a[irt][jrt] = 3;
                    }
                    this->addLine(i*40,-j*40,rtx*40,-(rty*40),QPen(Qt::black));
                }

                ty++;
                while(/*tz != z && */ty < y && a[i][ty] == tape)
                {
                    tz++;
                    ty++;
                }
                if(tz == z)
                {
                    for(int jt = j; jt < ty; jt++)
                    {
                        qDebug() << "i, jt << " << i << jt;
                        a[i][jt] = 3;
                    }
                    //() << "i, j << " << i << j;
                    this->addLine(i*40+20,-(j*40),i*40+20,-(ty*40),QPen(Qt::black));
                }

                ltx--;
                lty++;
                while(/*ltz != z && */ltx >= 0 && lty < y && a[ltx][lty] == tape)
                {
                    ltz++;
                    ltx--;
                    lty++;
                    //qDebug() << ltz;
                }
                if(ltz == z)
                {
                    for(int iltx = i, jlty = j; jlty < lty; jlty++, iltx--)
                    {
                        //qDebug() << "iltx, jlty << " << iltx << jlty;
                        a[iltx][jlty] = 3;
                    }
                    this->addLine(i*40+40,-(j*40),ltx*40+40,-(lty*40),QPen(Qt::black));
                }
            }
        }
    }
    return 0;
}

void mousescene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    emit signalTargetCoordinate(event->scenePos());
}

mousescene::~mousescene()
{

}

