#ifndef LOGADMIN_H
#define LOGADMIN_H

#include <QDialog>

namespace Ui {
class logAdmin;
}

class logAdmin : public QDialog
{
    Q_OBJECT

public:
    explicit logAdmin(QWidget *parent = nullptr);
    ~logAdmin();

private slots:
    void on_Ok_clicked();

    void on_Cansel_clicked();

private:
    Ui::logAdmin *ui;
};

#endif // LOGADMIN_H
