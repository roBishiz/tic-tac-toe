#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "scene.h"
#include "mousescene.h"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    int x, y, z;
    explicit Settings(QWidget *parent = nullptr);
    ~Settings();
    //mousescene *Gamescene;

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

signals:
    void EnterScene(mousescene *scene);

private:
    Ui::Settings *ui;
    mousescene *Gamescene;
};

#endif // SETTINGS_H
