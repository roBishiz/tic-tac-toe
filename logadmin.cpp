#include "logadmin.h"
#include "ui_logadmin.h"
#include <QMessageBox>
#include "regacc.h"

logAdmin::logAdmin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::logAdmin)
{
    ui->setupUi(this);
    ui->Password->setEchoMode(QLineEdit::Password);
}

void logAdmin::on_Ok_clicked()
{
    if(ui->Login->text() == "Boris" && ui->Password->text() == "12345")
    {
        QMessageBox::information(this, "Succses!", "You are logged in as admin!");
        close();
        regAcc test(this);
        test.setWindowTitle("Register");
        test.exec();
    }
}


logAdmin::~logAdmin()
{
    delete ui;
}

void logAdmin::on_Cansel_clicked()
{
    close();
}
