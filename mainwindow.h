#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mousescene.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionNew_game_triggered();
public slots:
    void reEnterScene(mousescene *scene);

private:
    Ui::MainWindow *ui;
    mousescene *sceneMouse;
};

#endif // MAINWINDOW_H
