#include "settings.h"
#include "ui_settings.h"
#include <QDebug>
#include <QMessageBox>

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
//    ui->Stroke_lenght->setValue(3);
//    ui->Slider_width->setValue(3);
//    ui->Slider_height->setValue(3);
    connect(ui->Stroke_lenght, SIGNAL(valueChanged(int)),ui->spinBox_1,SLOT(setValue(int)));
    connect(ui->Slider_width, SIGNAL(valueChanged(int)),ui->spinBox_2,SLOT(setValue(int)));
    connect(ui->Slider_height, SIGNAL(valueChanged(int)),ui->spinBox_3,SLOT(setValue(int)));
    connect(ui->spinBox_1, SIGNAL(valueChanged(int)),ui->Stroke_lenght,SLOT(setValue(int)));
    connect(ui->spinBox_2, SIGNAL(valueChanged(int)),ui->Slider_width,SLOT(setValue(int)));
    connect(ui->spinBox_3, SIGNAL(valueChanged(int)),ui->Slider_height,SLOT(setValue(int)));
    //connect(this, SIGNAL(EnterScene(mousescene *)), parent, SLOT(reEnterScene(mousescene *)));
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_pushButton_clicked()
{
    if(ui->Stroke_lenght->value() > ui->Slider_width->value() && ui->Stroke_lenght->value() > ui->Slider_height->value())
    {
        QMessageBox::warning(this, "Error!", "Stroke lenght more than width or height");
        return;
    }
    Gamescene = new mousescene();
    Gamescene->Mas(ui->Slider_width->value(), ui->Slider_height->value(), ui->Stroke_lenght->value());
    //qDebug() << 1.2;
    emit EnterScene(Gamescene);
    close();
}

void Settings::on_pushButton_2_clicked()
{
    close();
}
