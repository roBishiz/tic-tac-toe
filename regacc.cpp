#include "regacc.h"
#include "ui_regacc.h"
#include <QFile>
#include <QDataStream>
#include <QMessageBox>
#include <QDebug>

regAcc::regAcc(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::regAcc)
{
    ui->setupUi(this);
}

void regAcc::on_Ok_clicked()
{
    ui->Login->text();
    QFile file("file.dat");

    if(!file.open(QIODevice::WriteOnly | QIODevice::Append)){
        QMessageBox::warning(this, "Title", "File not open");
    }

    QDataStream out(&file);
    //QString tmp;
    out << ui->Login->text();
    //out << "\n";
    out << ui->Password->text();
    //while(!file.atEnd()){
    //out >> tmp;
    //qDebug() << tmp;
    //}
    //out << "\n";
    //file.flush();
    file.close();
    QMessageBox::information(this, "Succses!", "Account is created!");
    close();
}

void regAcc::on_Cansel_clicked()
{
    close();
}

regAcc::~regAcc()
{
    delete ui;
}
