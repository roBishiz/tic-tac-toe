#-------------------------------------------------
#
# Project created by QtCreator 2018-11-05T17:21:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TIK_TOK
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    mousescene.cpp \
    settings.cpp \
    regacc.cpp \
    logadmin.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    mousescene.h \
    settings.h \
    regacc.h \
    logadmin.h

FORMS    += mainwindow.ui \
    dialog.ui \
    settings.ui \
    regacc.ui \
    logadmin.ui
